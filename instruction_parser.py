import os
import re
import hexdump
import json
from pprint import pprint as pp

instruction_parsed = {
	
}

possible_operands = []

operand_regexp_length = re.compile(r"(\d{1,2})")



def parse_imm_index(op, mask, op_char='i'):
	op = op.replace('[', '').replace("]", '')
	print op, mask

	# because you can not implement f*cking vector properly : SYSCALL ['vector8'] 11010111111vvvvv00VVV00101100000
	if "vector8" == op:
		return "REQUIRED SPECIAL PROCESSING"

	search_mask = "({0}{{2,}})".format(op_char)
	

	print search_mask, mask,
	start = re.search(search_mask, mask)
	if not start:
		print "-----<> BAD PARSE <>-----"
		return
	op_len = len(start.group(1))
	
	imm_mask = (2 ** op_len - 1) << (len(mask) - op_len - start.start())	
	print bin(imm_mask)
	return imm_mask


def create_operands_parse_info(operands, mask):

	match, extract =  get_opcode_mask(mask)
	instruction = {
		"original mask" : mask,
		"extract mask"  : extract,
		"match mask"  : match,
	}
	instr_description = []
	op_letters = {
		"imm" : "i",
		"disp" : "d",
		"list" : "L",
		"cccc" : "c",
		"rh"   : "R",
		"rt"   : "w",
		"reg1" : "R",
		"reg2" : "r",		
		"reg3" : "[Ww]",
		"reg4" : "[wW]",
		"bit#3" : "b",		
		"vector" : "v"
	}

	register_index = 0
	for op in operands:			
		for k, v in op_letters.items():
			
			if k in op:
				print "\tOP->", k,
				instr_description.append({
						"op_name" : op,					
						"op_mask" : bin(parse_imm_index(op, mask, v)),
						"op_type" : "o_reg"
					})
				break
	instruction["arguments"] = instr_description
	return instruction

def get_opcode_mask(mask):
	print mask,
	compare_mask = re.sub(r"([^\d])", "0", mask)
	extract_mask = re.sub(r"([\d])", "1", mask)
	extract_mask = re.sub(r"([^\d])", "0", extract_mask)
	return (compare_mask, extract_mask)

unparsed_instructions = []

ops_table = {
	
}

for instruction in open(os.sys.argv[1], "r").readlines():
	#print instruction
	opcode, variables, mask1, mask2, mask3 = instruction[:-1].split('|')	
	operands = [x for x in re.findall(r"([^\,\s\-]+)", variables)[1:]] #re.sub(r'\d', "", x)
	possible_operands += operands
	if opcode not in ops_table:
		ops_table[opcode] = []
	
	
	mask = ''.join([mask1, mask2, mask3])

	if operands == []:
		if len(set(list(mask))) <= 2: # <= retarded way to check if mask contains only 1 and 0
			print opcode, operands, "\t", " --> no args, mask == ", mask
			pass
		else:
			unparsed_instructions.append(instruction)
			continue
	
	try:		
		print opcode, operands, mask
		print "-> OPCODE MATCH/EXTRACT",
		ops_table[opcode].append(create_operands_parse_info(operands, mask))

		print "\n"
	except Exception as e:
		print instruction
		print e
		unparsed_instructions.append(instruction)
		

with open("ops_table.json", "wb") as o:
	o.write(json.dumps(ops_table, indent=4))



print "UNPARSED"
for ui in unparsed_instructions:
	print ui[:-1]

