# RV850_decompile

Hi,
This is a very raw implementation of decompiler for RV850. 
It contains several script which was used to create instruction set itself (ops_table.json).
So, this is how it works.
1) I convert pdf with instructions description to txt file.
2) Using extract_info_from_spec.ipynb grep instructions data from the txt file.
	extract_info_from_spec.ipynb("RH850 G3M core manual.txt") --> rv_850.lst
3) Convert list of instructions to JSON file with relevant bit masks.
	instruction_parser.py ("rv_850.lst") --> ops_table.json
4) Use rv_850_parser.py script to decompile binary to disasm. 
	It is also contains function rv_decomp() which is used to place decompiled instructions in ida to make sure that IDA output is correct


Probably one day i am gonna to create a proper plugin for ida like NECromancer did.	

