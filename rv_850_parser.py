import os
import json
import hexdump

from struct import unpack, pack

ops_talbe = None
with open("./ops_table.json", "rb") as i:
	ops_table = json.loads(i.read())


def match_mask(value, match_mask, extract_mask):
	return (value & extract_mask) == match_mask

def make_a_choice(candidates):
	def criteria(candidate):
		#pp(candidate)
		return candidate["extract mask"].count('1')

	largest_mask_length = criteria(candidates[0][1])
	current_candidate = candidates[0]
	for opcode_name, description in candidates[1:]:
		if largest_mask_length < criteria(description):
			largest_mask_length = criteria(description)			
			current_candidate = [opcode_name, description]

	return current_candidate

def get_arg(hex_word, arg_mask):
	shift = 0
	temp_mask = arg_mask
	#print arg_mask
	while temp_mask[-1] == "0":
		temp_mask = temp_mask[:-1]
		shift += 1
	#print "Arg shift", shift
	return (hex_word & int(arg_mask, 2)) >> shift



def fmt_disasm(hex_word, hex_string, opcode_info):

	opcode, args = opcode_info
	print hexdump.dump(hex_string).ljust(12), ":", 

	if opcode in special_decoders:
		print ', '.join(special_decoders[opcode](hex_word, opcode_info))
		return

	print opcode,

	args_string = [	]
	for arg in args["arguments"]:		
		if "reg" in arg["op_name"]:
			args_string.append('r' + str(get_arg(hex_word, arg['op_mask'] )))		
		else:
			args_string.append(format(get_arg(hex_word, arg['op_mask']), 'X'))
	print ', '.join(args_string)
	return [opcode] + args_string



# We need special processor for Bcond and syscall instruction

def process_Bcond_opcode(hex_word, instruction_description):
	conditional_code = {
		0b1110 : "BGE", # (S xor OV) = 0 Greater than or equal to signed
		0b1111 : "BGT", # ( (S xor OV) or Z) = 0 Greater than signed
		0b0111 : "BLE", # ( (S xor OV) or Z) = 1 Less than or equal to signed
		0b0110 : "BLT", # (S xor OV) = 1 Less than signed
		0b1011 : "BH",  # (CY or Z) = 0 Higher (Greater than)
		0b0001 : "BL",  # CY = 1 Lower (Less than)
		0b0011 : "BNH", # (CY or Z) = 1 Not higher (Less than or equal)
		0b1001 : "BNL", # CY = 0 Not lower (Greater than or equal)
		0b0010 : "BE",  # Z = 1 Equal
		0b1010 : "BNE", # Z = 0 Not equal
		0b0001 : "BC",  # CY = 1 Carry
		0b1010 : "BF",  # Z = 0 False
		0b0100 : "BN",  # S = 1 Negative
		0b1001 : "BNC", # CY = 0 No carry
		0b1000 : "BNV", # OV = 0 No overflow
		0b1010 : "BNZ", # Z = 0 Not zero
		0b1100 : "BP",  # S = 0 Positive
		0b0101 : "BR",  # Always (Unconditional)
		0b1101 : "BSA", # SAT = 1 Saturated
		0b0010 : "BT",  # Z = 1 True
		0b0000 : "BV",  # OV = 1 Overflow
		0b0010 : "BZ",  # Z = 1 Zero
	}	
	#print instruction_description[1]
	bcond_version = len(instruction_description[1]["match mask"])
	if bcond_version  == 16:
		#print bin(hex_word)
		disp5 = get_arg(hex_word, "1111100000000000")
		disp3 = get_arg(hex_word, "1110000")
		disp  = int(format(disp5, '05b') + format(disp3, '03b') , 2) << 1
		#print format(disp5, '05b') , format(disp3, '03b')
		jmp_condition = get_arg(hex_word, "1111")
	else:
		disp1  = get_arg(hex_word, "100000000000000000000")
		disp15 = get_arg(hex_word, "1111111111111110")
		#print bin(disp1), bin(disp15)
		disp   = int(bin(disp15)[2:] + bin(disp1)[2:] , 2) << 1

		jmp_condition = get_arg(hex_word, "11110000000000000000")

	return [conditional_code[jmp_condition], format(disp, "X")]
	

def process_syscall_opcode(hex_word, instruction_description):
	vector5 = get_arg(hex_word, "111110000000000000000")
	vector3 = get_arg(hex_word, "11100000000000")
	disp = int(bin(vector5)[2:] + bin(vector3)[2:] , 2) << 1

	return ["SYSCALL", format(disp, "X")]

special_decoders = {
		"Bcond" : process_Bcond_opcode,		
		"SYSCALL" : process_syscall_opcode
}



def decode_instructions(hex_list):
	global ops_table

	hex_mask = {
		16 : 0,
		32 : 1,
		48 : 2
	}

	candidates = [

	]

	for opcode_name, descriptions in ops_table.items():
		for descr in descriptions:			
			match = int(descr["match mask"], 2)
			extract = int(descr["extract mask"], 2)
			original_mask = descr["original mask"]

			mask_line = len(original_mask)
			target_hex = hex_list[hex_mask[mask_line]]		

			if match_mask(target_hex, match, extract):										
				candidates.append([opcode_name, descr])

	result = None	

	if len(candidates) == 1:
		result = candidates[0]
	elif any(candidates):
		result = make_a_choice(candidates)
	else:		
		print "Disasm error for", format(word, "08X"), hexdump.dump(pack("<I", word))
		return None

	return result
	#fmt_disasm(result_hex, result_hex_string, result)


def obtain_size(hex_list, result):	
	masks_switch = {
		16 : [0, 2, "<H"],
		32 : [1, 4, "<I"],
		48 : [2, 6, "<Q"],
	}

	mask_len = len(result[1]["extract mask"])
	index, size, formater = masks_switch[mask_len]
	return (size, hex_list[index], pack(formater, hex_list[index]))

def rv_decomp():
	addr = ScreenEA()
	dsize = 10

	for x in range(dsize):	
		hex_code = GetManyBytes(addr, 6)
		hex_list = [
		 unpack("<H", hex_code[:2])[0],
		 unpack("<I", hex_code[2:4] + hex_code[:2])[0],
		 unpack("<Q", "\x00\x00" + hex_code[4:6] + hex_code[2:4] + hex_code[:2])[0]
		]

		result = decode_instructions(hex_list)	
		
		print "{0:0{1}x}|".format(addr, 8),
		
		if not result:
			addr = NextHead(addr)			
			continue

		opsize, result_hex, result_hex_string = obtain_size(hex_list, result)		
		res = fmt_disasm(result_hex, result_hex_string, result)
		if res:
			disasm_string = res[0] + " " + ', '.join(res[1:])		
			MakeComm(addr, str(disasm_string))

		addr += opsize

def main():
	hex_code = open(os.sys.argv[1], "rb").read()

	base = 0
	if len(os.sys.argv) > 2:
		base = int(os.sys.argv[2], 16)

	size = len(hex_code)
	size_padding = len(hex(size + base)[2:])
	if size_padding < 4:
		size_padding = 4

	i = 0
	while i < size - 6:
		
		hex_list = [
		 unpack("<H", hex_code[i:i+2])[0],
		 unpack("<I", hex_code[i+2:i+4] + hex_code[i:i+2])[0],
		 unpack("<Q", "\x00\x00" + hex_code[i+4:i+6] + hex_code[i+2:i+4] + hex_code[i:i+2])[0]
		]

		result = decode_instructions(hex_list)	
		
		print "{0:0{1}x}|".format(i + base, size_padding),
		
		if not result:
			i += 4			
			continue

		opsize, result_hex, result_hex_string = obtain_size(hex_list, result)
		i += opsize

		fmt_disasm(result_hex, result_hex_string, result)

if __name__ == '__main__':
	main()

